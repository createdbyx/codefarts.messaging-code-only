namespace Codefarts.Messaging
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Provides a base class for writing objects.
    /// </summary>
    /// <typeparam name="T">Specifies the type that is written.</typeparam>
    public abstract class WriterBase<T>
        where T : class
    {
        /// <summary>
        /// Writes an object to the source folder. 
        /// </summary>
        /// <param name="obj">Object to write to the source folder.</param>
        /// <exception cref="SerializationException">An object in the graph of type parameter <typeparamref name="T"/> is not marked as serializable.</exception>
        public abstract void WriteObject(T obj);
    }
}