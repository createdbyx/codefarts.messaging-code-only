﻿#if UNITY_5
namespace Codefarts.Messaging
{
    using System.IO;

    /// <summary>
    /// Deserializes binary data stored by a <see cref="FileMessageWriterBase{T}"/> implementation
    /// into a .NET CLR object specified by <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Reference type to deserialize data to.</typeparam>
    public class UnityJsonFileMessageReader<T> : FileMessageReaderBase<T>
        where T : class
    {
        /// <summary>
        /// Deserializes the file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        protected override T DeserializeFile(string path)
        {    
            return UnityEngine.JsonUtility.FromJson<T>(File.ReadAllText(path));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityJsonFileMessageReader{T}"/> class.
        /// </summary>
        /// <param name="sourceFolder">The source folder.</param>
        /// <param name="template">The template.</param>
        public UnityJsonFileMessageReader(string sourceFolder, string template)
            : base(sourceFolder, template)
        {
        }
    }
}
#endif