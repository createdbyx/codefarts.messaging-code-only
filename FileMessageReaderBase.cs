﻿namespace Codefarts.Messaging
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;

    /// <summary>
    /// Deserializes binary data stored by a <see cref="FileMessageWriterBase{T}"/> implementation
    /// into a .NET CLR object specified by <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Reference type to deserialize data to.</typeparam>
    public abstract class FileMessageReaderBase<T> : ReaderBase<T>
        where T : class
    {
        /// <summary>
        /// Gets the location of the source folder where messages are read from.
        /// </summary>
        public virtual string SourceFolder
        {
            get
            {
                return this.sourceFolder;
            }

            set
            {
                this.sourceFolder = value;
            }
        }

        /// <summary>
        /// Gets or sets the index that represents the next message id.
        /// </summary>
        public int Index
        {
            get
            {
                return this.index;
            }

            set
            {
                this.index = value;
            }
        }

        /// <summary>
        /// The index representing the sequential file number.
        /// </summary>
        protected int index;

        /// <summary>
        /// Holds the value for the <see cref="SourceFolder"/> property;
        /// </summary>
        protected string sourceFolder;

        /// <summary>
        /// Holds the value for the <see cref="Template"/> property;
        /// </summary>
        private string template;

        /// <summary>
        /// Constructs a new <c>FileMessageReader</c> object that reads data from the given <paramref name="sourceFolder"/>.
        /// </summary>
        /// <param name="sourceFolder">Location of the source folder.</param>
        /// <param name="template">The file name template.</param>
        protected FileMessageReaderBase(string sourceFolder, string template)
        {
            if (string.IsNullOrEmpty(sourceFolder))
            {
                throw new ArgumentNullException("sourceFolder");
            }

            if (string.IsNullOrEmpty(template))
            {
                throw new ArgumentNullException("template");
            }

            this.sourceFolder = sourceFolder;
            this.template = template;
        }

        /// <summary>
        /// Gets the filename template.
        /// </summary>                
        /// <remarks>Use {0} to specify the location of the index.</remarks>
        public virtual string Template
        {
            get
            {
                return this.template;
            }

            set
            {
                this.template = value;
            }
        }

        #region Private stream readers

        /// <summary>
        /// Tries to read the next object in the sequence.
        /// </summary>
        /// <returns>true if successfull; otherwise false.</returns>
        /// <exception cref="SerializationException">An object in the graph of type parameter <typeparamref name="T" /> is not marked as serializable.</exception>
        public override T ReadObject()
        {
            var path = Path.Combine(this.SourceFolder, string.Format(this.Template, this.index));
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }
                         
            var value = this.DeserializeFile(path);
            this.index++;
            return value;
        }

        /// <summary>
        /// Deserialize a file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <returns>Returns the deserialized result.</returns>
        protected abstract T DeserializeFile(string path);

        /// <summary>
        /// Tries to the read the next message sequence.
        /// </summary>
        /// <param name="value">The value to be return if successful.</param>
        /// <returns>true is successful; otherwise false.</returns>
        public override bool TryReadObject(out T value)
        {
            try
            {
                value = this.ReadObject();
                return true;
            }
            catch
            {
                value = default(T);
                return false;
            }
        }

        #endregion    
    }
}