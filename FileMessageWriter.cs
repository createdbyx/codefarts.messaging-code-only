namespace Codefarts.Messaging
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Serializes .NET CLR objects specified by <typeparamref name="T"/>
    /// into binary form and sends stores them in individual files located at a specified folder for a <see cref="FileMessageReaderBase{T}"/> implementation to read and deserialize.
    /// </summary>
    /// <typeparam name="T">Reference type to serialize.</typeparam>
    public class FileMessageWriter<T> : FileMessageWriterBase<T>
        where T : class
    {
        /// <summary>
        /// The binary formatter used to serialize objects.
        /// </summary>
        protected BinaryFormatter binaryFormatter = new BinaryFormatter();

        /// <summary>
        /// Serializes the object and saves it to a file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <param name="obj">THe object to be serialized.</param>
        protected override void SerializeFile(string path, T obj)
        {
            using (var stream = new FileStream(path, FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                this.binaryFormatter.Serialize(stream, obj);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileMessageWriter{T}"/> class.
        /// </summary>
        /// <param name="sourceFolder">The source folder.</param>
        /// <param name="template">The template.</param>
        public FileMessageWriter(string sourceFolder, string template)
            : base(sourceFolder, template)
        {
        }
    }
}