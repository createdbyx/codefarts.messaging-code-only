namespace Codefarts.Messaging
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Provides a base class for reading object types.
    /// </summary>
    /// <typeparam name="T">Specifies the type that is read.</typeparam>
    public abstract class ReaderBase<T>
        where T : class
    {
        /// <summary>
        /// Tries to read the next object in the sequence.
        /// </summary>
        /// <returns>true if successfull; otherwise false.</returns>
        /// <exception cref="SerializationException">An object in the graph of type parameter <typeparamref name="T" /> is not marked as serializable.</exception>
        public abstract T ReadObject();

        /// <summary>
        /// Tries to the read the next message sequence.
        /// </summary>
        /// <param name="value">The value to be return if successful.</param>
        /// <returns>true is successful; otherwise false.</returns>
        public abstract bool TryReadObject(out T value);
    }
}