#if UNITY_5
namespace Codefarts.Messaging
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Serializes .NET CLR objects specified by <typeparamref name="T"/>
    /// into binary form and sends stores them in individual files located at a specified folder for a <see cref="FileMessageReaderBase{T}"/> implementation to read and deserialize.
    /// </summary>
    /// <typeparam name="T">Reference type to serialize.</typeparam>
    public class UnityJsonFileMessageWriter<T> : FileMessageWriterBase<T>
        where T : class
    {
        /// <summary>
        /// Serializes the object and saves it to a file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <param name="obj">THe object to be serialized.</param>
        protected override void SerializeFile(string path, T obj)
        {
            File.WriteAllText(path, UnityEngine.JsonUtility.ToJson(obj, !this.Mininmize));
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="UnityJsonFileMessageWriter{T}"/> will save the json mininmized.
        /// </summary>
        public virtual bool Mininmize { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityJsonFileMessageWriter{T}"/> class.
        /// </summary>
        /// <param name="sourceFolder">The source folder.</param>
        /// <param name="template">The template.</param>
        public UnityJsonFileMessageWriter(string sourceFolder, string template)
            : base(sourceFolder, template)
        {
        }
    }
} 
#endif