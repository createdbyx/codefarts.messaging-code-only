﻿namespace Codefarts.Messaging
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Deserializes binary data stored by a <see cref="FileMessageWriter{T}"/> implementation
    /// into a .NET CLR object specified by <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T">Reference type to deserialize data to.</typeparam>
    public class FileMessageReader<T> : FileMessageReaderBase<T>
        where T : class
    {
        /// <summary>
        /// The binary formatter used to deserialize objects.
        /// </summary>
        protected BinaryFormatter binaryFormatter = new BinaryFormatter();

        /// <summary>
        /// Deserialize a file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <returns>Returns the deserialized result.</returns>
        protected override T DeserializeFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return (T)this.binaryFormatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileMessageReader{T}"/> class.
        /// </summary>
        /// <param name="sourceFolder">The source folder.</param>
        /// <param name="template">The template.</param>
        public FileMessageReader(string sourceFolder, string template)
            : base(sourceFolder, template)
        {
        }
    }
}
