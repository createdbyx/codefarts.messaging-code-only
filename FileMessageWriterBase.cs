namespace Codefarts.Messaging
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;

    /// <summary>
    /// Serializes .NET CLR objects specified by <typeparamref name="T"/>
    /// into binary form and sends stores them in individual files located at a specified folder for a <see cref="FileMessageReader{T}"/> to read and deserialize.
    /// </summary>
    /// <typeparam name="T">Reference type to serialize.</typeparam>
    public abstract class FileMessageWriterBase<T> : WriterBase<T>
        where T : class
    {
        /// <summary>
        /// Gets the path to the source folder where serialized objects will be stored in.
        /// </summary>
        public virtual string SourceFolder
        {
            get
            {
                return this.sourceFolder;
            }

            set
            {
                this.sourceFolder = value;
            }
        }

        /// <summary>
        /// Gets or sets the index that represents the next message id.
        /// </summary>
        public int Index
        {
            get
            {
                return this.index;
            }

            set
            {
                this.index = value;
            }
        }

        /// <summary>
        /// The index representing the sequential file number.
        /// </summary>
        protected int index;

        /// <summary>
        /// Holds the value for the <see cref="SourceFolder"/> property;
        /// </summary>
        protected string sourceFolder;

        /// <summary>
        /// Holds the value for the <see cref="Template"/> property;
        /// </summary>
        private string template;

        /// <summary>
        /// Constructs a new <c>FileMessageWriter</c> object that writes to given <paramref name="sourceFolder"/>.
        /// </summary>
        /// <param name="sourceFolder">The location of the source folder.</param>
        /// <param name="template">The file name template.</param>
        public FileMessageWriterBase(string sourceFolder, string template)
        {
            if (string.IsNullOrEmpty(sourceFolder))
            {
                throw new ArgumentNullException("sourceFolder");
            }

            if (!Directory.Exists(sourceFolder))
            {
                throw new DirectoryNotFoundException("'sourceFolder' does not exist!");
            }

            if (string.IsNullOrEmpty(template))
            {
                throw new ArgumentNullException("template");
            }

            this.sourceFolder = sourceFolder;
            this.template = template;
        }

        /// <summary>
        /// Gets the filename template.
        /// </summary>                
        /// <remarks>Use {0} to specify the location of the index.</remarks>
        public virtual string Template
        {
            get
            {
                return this.template;
            }

            set
            {
                this.template = value;
            }
        }

        /// <summary>
        /// Writes an object to the source folder. 
        /// </summary>
        /// <param name="obj">Object to write to the source folder.</param>
        /// <exception cref="SerializationException">An object in the graph of type parameter <typeparamref name="T"/> is not marked as serializable.</exception>
        public override void WriteObject(T obj)
        {
            var path = Path.Combine(this.SourceFolder, string.Format(this.Template, this.index));
            this.SerializeFile(path, obj);
            this.index++;
        }

        /// <summary>
        /// Serializes the object and saves it to a file.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <param name="obj">THe object to be serialized.</param>
        protected abstract void SerializeFile(string path, T obj);
    }
}